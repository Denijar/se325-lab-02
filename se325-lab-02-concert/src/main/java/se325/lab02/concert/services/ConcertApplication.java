package se325.lab02.concert.services;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/services")
public class ConcertApplication extends Application {

    private Set<Object> singletons = new HashSet<>();

    // Instances from this list get instantiated as necessary
    // It uses an instance of SerializedMessageBodyReaderAndWriter to convert java objects to and from their bytecode representation
    private Set<Class<?>> classes = new HashSet<>();

    public ConcertApplication(){
        singletons.add(new ConcertResource());
        classes.add(SerializationMessageBodyReaderAndWriter.class);
    }

    @Override
    public Set<Object> getSingletons(){
        return singletons;
    }

    @Override
    public Set<Class<?>> getClasses(){
        return classes;
    }

}
